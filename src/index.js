import React from "react";
import ReactDOM from "react-dom";
import App from "./App/App";
import "handsontable/dist/handsontable.full.css";

ReactDOM.render(<App />, document.getElementById("root"));
