import React from "react";
import { HotTable } from "@handsontable/react";
import Handsontable from "handsontable";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.id = "hot";
    this.handsontableData = Handsontable.helper.createSpreadsheetData(6, 10);
    this.hotTableComponent = React.createRef();
  }

  componentDidMount() {
    this.exportPlugin = this.hotTableComponent.current.hotInstance.getPlugin(
      "exportFile"
    );
  }

  downloadCSV() {
    this.exportPlugin.downloadFile("csv", {
      bom: false,
      columnDelimiter: ",",
      columnHeaders: false,
      exportHiddenColumns: true,
      exportHiddenRows: true,
      fileExtension: "csv",
      filename: "UXPIN_CSV-file_[YYYY]-[MM]-[DD]",
      mimeType: "text/csv",
      rowDelimiter: "\r\n",
      rowHeaders: true
    });
  }

  render() {
    return (
      <div>
        <HotTable
          ref={this.hotTableComponent}
          id={this.id}
          settings={{
            data: this.handsontableData,
            rowHeaders: true,
            colHeaders: true,
            dropdownMenu: true,
            contextMenu: true,
            exportFile: true,
            licenseKey: "non-commercial-and-evaluation"
          }}
        />
        <button onClick={this.downloadCSV.bind(this)}>Download CSV</button>
      </div>
    );
  }
}

export default App;
